import BaseController from "./base_controller"
import cable from '../lib/actioncable'
import api from '../lib/api'

export default class extends BaseController {
  initialize () {
    this.eventBus.addEventListener('comments:created', function (payload) {
      this.handleCommentsCreated(payload)
    }.bind(this))

    this._fetchUnreadNotificationsCount()
    this._setupCable()
  }

  _fetchUnreadNotificationsCount () {
    let req = api.unreadNotificationsCount()

    req.then((resp) => {
      this.unreadCount = resp.data.count
    })
  }

  _refreshBadge () {
    if (this.unreadCount > 0) {
      this.element.classList.add('badge')
    } else {
      this.element.classList.remove('badge')
    }
  }

  _refreshHref () {
    if (this.unreadCount > 0) {
      this.element.setAttribute('href', this.unreadPath)
    } else {
      this.element.setAttribute('href', this.allPath)
    }
  }

  _refreshFavicon () {
    this.faviconController.hasUnread = this.unreadCount > 0
  }

  _setupCable() {
    let _this = this

    let actions = {
      received(payload) {
        switch (payload.event) {
          case 'notifications.created':
            _this.unreadCount++
            break;

          case 'notifications.marked_all_as_read':
            _this.unreadCount = 0
            break;
        }
      }
    }

    cable.subscriptions.create('NotificationsChannel', actions)
  }

  get allPath () {
    return this.data.get('all-path')
  }

  get unreadPath() {
    return this.data.get('unread-path')
  }

  get unreadCount () {
    return parseInt(this.data.get('unread-count'))
  }

  set unreadCount (value) {
    this.data.set('unread-count', value)

    this._refreshBadge()
    this._refreshHref()
    this._refreshFavicon()
  }

  get faviconController () {
    return this.application.getControllerForElementAndIdentifier(document.getElementById('favicon'), "favicon")
  }
}
