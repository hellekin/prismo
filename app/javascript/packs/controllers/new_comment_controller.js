import BaseController from "./base_controller"
import autosize from 'autosize'

export default class extends BaseController {
  static targets = ['body']

  connect () {
    this.element.addEventListener('ajax:success', function(e) { this.onCommentSubmitSuccess(e) }.bind(this))
    this.element.addEventListener('ajax:error', function (e) { this.onCommentSubmitError(e) }.bind(this))

    autosize(this.bodyTarget)
  }

  disconnect () {
    this.element.removeEventListener('ajax:success', function (e) { this.onCommentSubmitSuccess(e) }.bind(this))
    this.element.removeEventListener('ajax:error', function (e) { this.onCommentSubmitError(e) }.bind(this))

    autosize.destroy(this.bodyTarget)
  }

  onCommentSubmitSuccess (resp) {
    // `create` mode
    if (this.mode == 'create') {
      this.eventBus.dispatch('comments:created', { response: resp.detail })
      this.addToast({ text: 'Comment has been added', severity: 'success' })
      this.element.reset()

      if (this.element.getAttribute('data-parent-id')) {
        this._afterCreatedChildComment()
      } else {
        this._afterCreatedRootComment()
      }

    // `update` mode
    } else {
      this.eventBus.dispatch('comments:updated', { response: resp.detail })
      this.addToast({ text: 'Comment has been updated', severity: 'success' })
      this.element.reset()
    }
  }

  onCommentSubmitError(resp) {
    let status = resp.detail[2].status
    this._resolveRequestErrorStatus(status)

    if (status == 400) {
      this.element.outerHTML = resp.detail[0].getElementsByClassName('new_comment')[0].outerHTML
    }
  }

  _afterCreatedRootComment () {
    // logic goes here
  }

  _afterCreatedChildComment() {
    this.element.remove()
  }

  get mode () {
    return this.element.getAttribute('data-mode')
  }
}
