import { Controller } from "stimulus"
import EventBus from '../lib/eventbus'
import errorStatusResolver from '../lib/error_status_resolver'

export default class extends Controller {
  addToast (payload) {
    EventBus.addToast(payload)
  }

  _resolveRequestErrorStatus(status) {
    errorStatusResolver(status)
  }

  get eventBus () {
    return EventBus
  }

  get currentAccountId () {
    return parseInt(document.head.querySelector('[name="current-account-id"]').content)
  }
}
