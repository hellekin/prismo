import axios from 'axios'

export default {
  unreadNotificationsCount () {
    return axios.get('/api/v1/notifications/unread_count')
  },

  tags (params = {}) {
    return axios.get('/api/v1/tags', { params: params })
  },
}
