class ActivityPub::UndoLikeSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: id,
      type: 'Undo',
      object: ActivityPub::LikeSerializer.new(object).data,
      actor: ActivityPub::TagManager.instance.uri_for(object.account)
    }
  end

  def id
    [ActivityPub::TagManager.instance.uri_for(object.account), '#likes/', object.id, '/undo'].join
  end
end
