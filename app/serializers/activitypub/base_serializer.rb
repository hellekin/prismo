class ActivityPub::BaseSerializer < Granola::Serializer
  include RoutingHelper

  CONTEXT = [
    'https://www.w3.org/ns/activitystreams',
    'https://w3id.org/security/v1',

    {
      'Hashtag' => 'as:Hashtag'
    }, {
      'votes' => {
        '@id'   => 'as:votes',
        '@type' => '@id'
      }
    }
  ].freeze

  attr_reader :options

  def initialize(object, options = {})
    @options = options
    super(object)
  end

  def as_json(opts = {})
    options.merge!(opts)

    json = data
    json['@context'] = CONTEXT if options[:with_context]

    json
  end

  def to_json(opts = {})
    Oj.dump(as_json(opts))
  end
end
