class Vote < ApplicationRecord
  belongs_to :voteable, polymorphic: true, counter_cache: true
  belongs_to :account
end
