class Stories::UpdatePolicy < StoryPolicy
  def update_url?
    user.is_admin?
  end
end
