class Stories::Vote < ActiveInteraction::Base
  object :story
  object :account

  def to_model
    ::Vote.new
  end

  def execute
    return existing_vote if existing_vote.present?

    vote = ::Vote.new(voteable: story)
    vote.account = account

    if vote.save
      account.touch(:last_active_at)
      # Stories::BroadcastChanges.run! story: story
    else
      errors.merge!(vote.errors)
    end

    vote
  end

  private

  def existing_vote
    @existing_vote ||= ::Vote.find_by(voteable: story, account: account)
  end
end
