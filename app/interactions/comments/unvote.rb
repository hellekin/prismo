class Comments::Unvote < ActiveInteraction::Base
  object :comment
  object :account

  def execute
    vote = ::Vote.find_by(account: account, voteable: comment)

    if vote.present?
      vote.destroy
      ActivityPub::UndoVoteDistributionJob.call(vote) if !comment.account.local?
    end
    # Stories::BroadcastChanges.run! story: story.reload

    vote
  end
end
