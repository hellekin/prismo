class Settings::ProfilesController < ApplicationController
  before_action :authenticate_user!

  layout 'settings'

  def show
    @account = current_user.account
  end

  def update
    @account = current_user.account

    if @account.update_attributes(account_params)
      redirect_to settings_profile_path, notice: I18n.t('generic.changes_saved_msg')
    else
      render :show
    end
  end

  private

  def account_params
    params.require(:account).permit(:display_name, :bio, :avatar, :remove_avatar)
  end
end
