class Admin::SettingsController < ApplicationController
  before_action :authenticate_user!

  layout 'settings'

  ADMIN_SETTINGS = %w(
    site_title
    site_description
    stories_per_day
    story_votes_per_day
    comment_votes_per_day
    open_registrations
  ).freeze

  BOOLEAN_SETTINGS = %w(open_registrations).freeze
  UPLOAD_SETTINGS = %w().freeze

  def edit
    authorize :admin
    @admin_settings = Form::AdminSettings.new
  end

  def update
    authorize :admin

    settings_params.each do |key, value|
      if UPLOAD_SETTINGS.include?(key)
        upload = SiteUpload.where(var: key).first_or_initialize(var: key)
        upload.update(file: value)
      else
        setting = Setting.where(var: key).first_or_initialize(var: key)
        setting.update(value: value_for_update(key, value))
      end
    end

    flash[:notice] = I18n.t('generic.changes_saved_msg')
    redirect_to edit_admin_settings_path
  end

  private

  def settings_params
    params.require(:form_admin_settings).permit(ADMIN_SETTINGS)
  end

  def value_for_update(key, value)
    if BOOLEAN_SETTINGS.include?(key)
      value == '1'
    else
      value
    end
  end
end
