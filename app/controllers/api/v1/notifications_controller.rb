class Api::V1::NotificationsController < Api::V1Controller
  before_action :authenticate_user!

  def unread_count
    notifications = current_account.notifications_as_recipient.not_seen
    render json: { count: notifications.count }
  end
end
