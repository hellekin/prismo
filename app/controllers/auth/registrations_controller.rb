class Auth::RegistrationsController < Devise::RegistrationsController
  before_action :check_enabled_registrations, only: [:new, :create]
  before_action :configure_sign_up_params, only: [:create]
  # before_action :set_sessions, only: [:edit, :update]
  # before_action :set_instance_presenter, only: [:new, :create, :update]

  protected

  def build_resource(hash = nil)
    super(hash)

    # resource.locale      = I18n.locale
    # resource.invite_code = params[:invite_code] if resource.invite_code.blank?

    resource.build_account if resource.account.nil?
  end

  def check_enabled_registrations
    redirect_to :root, notice: 'Registration is currently disabled' if !Setting.open_registrations
  end

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up) do |u|
      u.permit({ account_attributes: [:username] }, :email, :password, :password_confirmation)
    end
  end
end
