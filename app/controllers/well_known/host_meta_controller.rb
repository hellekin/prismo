module WellKnown
  class HostMetaController < ActionController::Base
    def show
      response.headers['Vary'] = 'Accept'
      @webfinger_template = "#{webfinger_url}?resource={uri}"

      respond_to do |format|
        format.xml { render content_type: 'application/xrd+xml' }
      end
    end
  end
end
