require "image_processing/mini_magick"

class UrlMetaThumbUploader < Shrine
  plugin :processing
  plugin :versions
  plugin :remote_url, max_size: 20*1024*1024

  process(:cache) do |io, _|
    pipeline = ImageProcessing::Vips.source(io)
                                    .loader(page: 1)
                                    .convert('jpg')
                                    .saver(background: 255, quality: 100)

    pipeline.resize_to_limit!(990, 990)
  end

  process(:store) do |io, _context|
    original = io.download
    pipeline = ImageProcessing::Vips.source(original)
                                    .saver(quality: 80)

    size_200 = pipeline.resize_to_fill!(200, 200, crop: :attention)

    original.close!

    {
      original: io,
      size_200: size_200
    }

  end
end
