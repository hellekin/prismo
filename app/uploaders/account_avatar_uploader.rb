require "image_processing/mini_magick"

class AccountAvatarUploader < Shrine
  plugin :processing
  plugin :versions
  plugin :remote_url, max_size: 20*1024*1024

  process(:store) do |io, _context|
    original = io.download
    pipeline = ImageProcessing::Vips.source(original)

    size_60  = pipeline.resize_to_fill!(60, 60)
    size_400 = pipeline.resize_to_fill!(400, 400)

    original.close!

    {
      size_60: size_60,
      size_400: size_400
    }
  end
end
