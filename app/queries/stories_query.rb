class StoriesQuery
  attr_reader :relation

  def initialize(relation = Story.all)
    @relation = relation
  end

  def with_includes
    relation.includes(:account, :url_meta, :tags)
  end

  def hot
    with_includes.order(Arel.sql('ranking(votes_count, created_at::timestamp, 3) DESC'))
  end

  def recent
    with_includes.order(created_at: :desc)
  end

  def for_account(account)
    with_includes.where(account_id: account.id)
  end

  def tagged_with(tags)
    with_includes.tagged_with(names: tags)
  end
end
