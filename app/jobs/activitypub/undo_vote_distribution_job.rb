class ActivityPub::UndoVoteDistributionJob < ApplicationJob
  queue_as :push

  def self.call(vote)
    ActivityPub::UndoVoteDistributionJob.perform_later(
      vote.id,
      vote.voteable_type,
      vote.voteable_id,
      vote.account_id
    )
  end

  def perform(vote_id, voteable_type, voteable_id, account_id)
    @vote_id = vote_id
    @voteable = voteable_type.constantize.find(voteable_id)
    @account = Account.find(account_id)

    inboxes.each do |inbox_url|
      ActivityPub::DeliveryJob.perform_later(
        signed_payload, account.id, inbox_url
      )
    end

  rescue ActiveRecord::RecordNotFound
    true
  end

  private

  attr_reader :voteable, :account

  def inboxes
    if voteable.is_a?(Comment)
      [voteable.account.inbox_url]
    else
      []
    end
  end

  def signed_payload
    @signed_payload ||= Oj.dump(ActivityPub::LinkedDataSignature.new(payload).sign!(account))
  end

  def payload
    @payload ||= ActivityPub::UndoLikeSerializer.new(voteable, with_context: true).as_json
  end
end
