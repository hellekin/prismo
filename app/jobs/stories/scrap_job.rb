require 'open-uri'

class Stories::ScrapJob < ApplicationJob
  include ActionView::Helpers::TextHelper

  queue_as :default

  def perform(story_id)
    @attributes = {}
    @story = Story.find(story_id)
    return if story.url.blank?

    thumbnailer
    story.touch(:scrapped_at)

    set_basic_attributes!
    set_thumb_attributes!

    save_url_meta!

    Stories::BroadcastChanges.run! story: story
  end

  private

  attr_reader :story, :attributes

  def set_basic_attributes!
    @attributes.merge!(
      title:       strip_tags(thumbnailer.title),
      description: strip_tags(thumbnailer.description)
    )
  end

  def set_thumb_attributes!
    return if thumbnailer.images.empty?

    thumb_uri = thumbnailer&.images&.first&.src
    return if thumb_uri.nil?

    @attributes.merge! thumb_remote_url: thumb_uri
  end

  def save_url_meta!
    url_meta = story.url_meta || UrlMeta.new

    url_meta.update_attributes(attributes)
    story.update(url_meta: url_meta)
  end

  def thumbnailer
    @thumbnailer ||= LinkThumbnailer.generate(story.url)
  end
end
