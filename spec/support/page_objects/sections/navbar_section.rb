class NavbarSection < SitePrism::Section
  element :add_story_link, 'a', text: '+ Add story'
  element :comments_link, 'a', text: 'Comments'
end
