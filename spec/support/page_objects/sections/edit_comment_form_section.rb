class EditCommentFormSection < SitePrism::Section
  element :body_input, 'form#edit_comment [name="comment[body]"]'
  element :submit_btn, 'input[type="submit"]'

  def edit_comment(body: nil)
    body_input.set body
    submit_btn.click
  end
end
