require "capybara/rails"
require "capybara/rspec"
require 'capybara/email/rspec'
require 'capybara-screenshot/rspec'
require "selenium-webdriver"

Capybara::Screenshot.prune_strategy = :keep_last_run
Capybara.always_include_port = true

Capybara.configure do |config|
  config.server_port = 51755
  config.javascript_driver = :selenium
  config.default_max_wait_time = 10
end

# That's a hack to make capybara open emails in a proper host/port environment
Prismo::Application.config.action_mailer.default_url_options[:host] = "127.0.0.1:#{Capybara.server_port}"
