Fabricator(:user) do
  email { sequence { |i| "email#{i}@example.com" } }
  password 'TestPass'

  account
end

Fabricator(:user_admin, from: :user) do
  is_admin true
end
