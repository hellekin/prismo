require 'rails_helper'

feature 'Voting on story' do
  let(:sign_in_page) { SignInPage.new }
  let(:home_page) { HomePage.new }

  let(:user) { Fabricate(:user, password: 'TestPass', confirmed_at: Time.zone.now) }
  let!(:story) { Fabricate(:story) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  scenario 'signed in user votes on story', js: true do
    home_page.load

    sign_user_in
    expect(home_page).to have_stories

    # Vote
    story_el = home_page.stories.first
    story_el.click_vote_button
    sleep 2

    # Check value and unvote
    story_el = home_page.stories.first
    expect(story_el.votes_count.text).to eq '1'
    story_el.click_vote_button
    sleep 2

    # Check value and vote again
    story_el = home_page.stories.first
    expect(story_el.votes_count.text).to eq '0'
    story_el.click_vote_button
    sleep 2

    # Check value
    story_el = home_page.stories.first
    expect(story_el.votes_count.text).to eq '1'
  end

  scenario 'guest tries to vote on a story', js: true do
    home_page.load
    expect(home_page).to have_stories

    # Vote
    story_el = home_page.stories.first
    story_el.click_vote_button
    sleep 2

    expect(home_page).to have_content 'You need to be signed in to do this'
  end
end
