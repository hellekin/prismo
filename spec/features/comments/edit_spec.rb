require 'rails_helper'

feature 'Editing comment' do
  let(:story_page) { Stories::ShowPage.new }
  let(:sign_in_page) { SignInPage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass') }
  let!(:story) { create(:story) }
  let!(:comment) { create(:comment, story: story, account: user.account) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  scenario 'signed in user wants to edit his comment', js: true do
    sign_user_in
    story_page.load(id: story.id)

    # Check if story page has all the commenting-related sections
    expect(story_page).to be_displayed
    expect(story_page).to have_stories
    expect(story_page).to have_comments_tree
    expect(story_page.comments_tree).to have_root_comment_form

    expect(story_page.comments_tree).to have_comments
    first_comment = story_page.comments_tree.comments.first

    first_comment.edit_btn.click
    first_comment.wait_for_edit_comment_form
    expect(first_comment).to have_edit_comment_form
    edit_comment_form = first_comment.edit_comment_form

    edit_comment_form.edit_comment(body: 'Example sub-comment body - changed')

    sleep 2
    first_comment = story_page.comments_tree.comments.first
    expect(first_comment.body.text).to eq 'Example sub-comment body - changed'
    expect(Comment.last.body).to eq 'Example sub-comment body - changed'
    expect(story_page).to have_content 'Comment has been updated'
  end
end
