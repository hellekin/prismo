require 'rails_helper'

describe Comments::Delete do
  describe '#run' do
    subject { described_class.run(comment: comment) }
    let(:comment) { create(:comment, :remote) }

    it { expect { subject }.to change(comment, :removed).to(true) }
    it { expect { subject }.to change(comment, :account).to(nil) }
    it { expect { subject }.to change(comment, :body).to(nil) }
    it { expect { subject }.to change(comment, :body_html).to(nil) }
    it { expect { subject }.to change(comment, :uri).to(nil) }
    it { expect { subject }.to change(comment, :url).to(nil) }
    it { expect { subject }.to change(comment, :domain).to(nil) }
  end
end
