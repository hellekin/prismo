require "rails_helper"

describe Stories::Create do
  let(:inputs) { valid_inputs }
  let(:account) { Fabricate(:account) }
  let!(:group) { Fabricate(:group, supergroup: true) }
  let(:valid_inputs) do
    {
      url: 'http://example.com',
      title: 'Sample story',
      tag_list: 'foo, bar',
      description: 'Sample description',
      account: account
    }
  end

  let(:outcome) { described_class.run(inputs) }
  let(:outcome!) { described_class.run!(inputs) }
  let(:result) { outcome.result }
  let(:errors) { outcome.errors }

  before do
    allow(Stories::ScrapJob).to receive(:perform_later)
  end

  context 'when inputs are valid' do
    let(:inputs) { valid_inputs }
    it { expect(outcome).to be_valid }
  end

  context 'when url and description are blank' do
    let(:inputs) do
      valid_inputs.merge(
        url: nil,
        description: nil
      )
    end

    it { expect(outcome).to be_invalid }
  end

  context 'when there are less tags than set in settings' do
    let(:inputs) do
      valid_inputs.merge(
        tag_names: ['foo']
      )
    end

    before { Setting.min_story_tags = 10 }
    after { Setting.destroy :min_story_tags }

    it { expect(outcome).to be_invalid }
  end

  context 'when min_story_tags setting is set to 0' do
    let(:inputs) do
      valid_inputs.merge(
        tag_names: []
      )
    end

    before { Setting.min_story_tags = 0 }
    after { Setting.destroy :min_story_tags }

    it 'does not validate tags count' do
      expect(outcome).to be_valid
    end
  end

  context 'when URL is invalid' do
    let(:inputs) do
      valid_inputs.merge(
        url: 'wrongurl.com/xxx'
      )
    end

    it { expect(outcome).to be_invalid }
  end

  context 'when URL is taken' do
    before { Fabricate(:story, url: valid_inputs[:url]) }
    let(:inputs) { valid_inputs }

    it { expect(outcome).to_not be_valid }
  end

  describe 'setting domain' do
    let(:inputs) do
      valid_inputs.merge(
        url: 'https://prismo.news/story/123'
      )
    end

    it 'sets domain if url is present' do
      expect(result.url_domain).to eq 'prismo.news'
    end
  end

  describe 'hooks' do
    context 'when created' do
      it 'enqueues Stories::ScrapJob' do
        expect(Stories::ScrapJob).to receive(:perform_later)
        outcome!
      end

      it 'casts vote by author' do
        expect { outcome! }.to change { account.upvoted_stories.count }.by(1)
      end

      it 'enqueues ActivityPub::DistributionJob' do
        expect(ActivityPub::DistributionJob).to receive(:perform_later)
        outcome!
      end
    end
  end

  describe 'assigning to supergroup' do
    it 'assigns story to supergroup' do
      outcome!
      expect(result.group).to eq group
    end
  end
end
