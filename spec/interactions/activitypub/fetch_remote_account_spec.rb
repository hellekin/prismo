require 'rails_helper'

describe ActivityPub::FetchRemoteAccount do
  let!(:actor) do
    {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: 'https://example.com/admin',
      type: 'Person',
      preferredUsername: 'admin',
      name: 'Admin',
      summary: 'Admin of example.com instance',
      inbox: 'http://example.com/admin/inbox',
      url: 'https://example.com/admin'
    }
  end

  describe '#run' do
    let(:outcome) { described_class.run(uri: 'https://example.com/admin', id: true) }

    shared_examples 'sets profile data' do
      it 'returns an account' do
        expect(outcome.result).to be_an Account
      end

      it 'sets display name' do
        expect(outcome.result.display_name).to eq 'Admin'
      end

      it 'sets note' do
        expect(outcome.result.bio).to eq 'Admin of example.com instance'
      end

      it 'sets URL' do
        expect(outcome.result.url).to eq 'https://example.com/admin'
      end
    end

    context 'when the account does not have a inbox' do
      let!(:webfinger) { { subject: 'acct:admin@example.com', links: [{ rel: 'self', href: 'https://example.com/admin' }] } }

      before do
        actor[:inbox] = nil

        stub_request(:get, 'https://example.com/admin').to_return(body: Oj.dump(actor))
        stub_request(:get, 'https://example.com/.well-known/webfinger?resource=acct:admin@example.com').to_return(body: Oj.dump(webfinger), headers: { 'Content-Type': 'application/jrd+json' })
      end

      it 'fetches resource' do
        outcome
        expect(a_request(:get, 'https://example.com/admin')).to have_been_made.once
      end

      it 'looks up webfinger' do
        outcome
        expect(a_request(:get, 'https://example.com/.well-known/webfinger?resource=acct:admin@example.com')).to have_been_made.once
      end

      it 'returns nil' do
        expect(outcome.result).to be_nil
      end
    end

    context 'when URI and WebFinger share the same host' do
      let!(:webfinger) { { subject: 'acct:admin@example.com', links: [{ rel: 'self', href: 'https://example.com/admin' }] } }

      before do
        stub_request(:get, 'https://example.com/admin').to_return(body: Oj.dump(actor))
        stub_request(:get, 'https://example.com/.well-known/webfinger?resource=acct:admin@example.com').to_return(body: Oj.dump(webfinger), headers: { 'Content-Type': 'application/jrd+json' })
      end

      it 'fetches resource' do
        outcome
        expect(a_request(:get, 'https://example.com/admin')).to have_been_made.once
      end

      it 'looks up webfinger' do
        outcome
        expect(a_request(:get, 'https://example.com/.well-known/webfinger?resource=acct:admin@example.com')).to have_been_made.once
      end

      it 'sets username and domain from webfinger' do
        expect(outcome.result.username).to eq 'admin'
        expect(outcome.result.domain).to eq 'example.com'
      end

      include_examples 'sets profile data'
    end

    context 'when WebFinger presents different domain than URI' do
      let!(:webfinger) { { subject: 'acct:admin@iscool.af', links: [{ rel: 'self', href: 'https://example.com/admin' }] } }

      before do
        stub_request(:get, 'https://example.com/admin').to_return(body: Oj.dump(actor))
        stub_request(:get, 'https://example.com/.well-known/webfinger?resource=acct:admin@example.com').to_return(body: Oj.dump(webfinger), headers: { 'Content-Type': 'application/jrd+json' })
        stub_request(:get, 'https://iscool.af/.well-known/webfinger?resource=acct:admin@iscool.af').to_return(body: Oj.dump(webfinger), headers: { 'Content-Type': 'application/jrd+json' })
      end

      it 'fetches resource' do
        outcome.result
        expect(a_request(:get, 'https://example.com/admin')).to have_been_made.once
      end

      it 'looks up webfinger' do
        outcome.result
        expect(a_request(:get, 'https://example.com/.well-known/webfinger?resource=acct:admin@example.com')).to have_been_made.once
      end

      it 'looks up "redirected" webfinger' do
        outcome.result
        expect(a_request(:get, 'https://iscool.af/.well-known/webfinger?resource=acct:admin@iscool.af')).to have_been_made.once
      end

      it 'sets username and domain from final webfinger' do
        expect(outcome.result.username).to eq 'admin'
        expect(outcome.result.domain).to eq 'iscool.af'
      end

      include_examples 'sets profile data'
    end
  end
end
