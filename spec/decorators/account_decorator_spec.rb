require 'rails_helper'

describe AccountDecorator do
  let(:account) { create(:account) }
  let(:decorator) { account.decorate  }

  describe '#bio_html' do
    subject { decorator.bio_html }

    context 'when bio is nil' do
      let(:account) { build(:account, bio: nil) }
      it { is_expected.to be nil }
    end

    context 'when bio is empty' do
      let(:account) { build(:account, bio: '') }
      it { is_expected.to be nil }
    end
  end
end
