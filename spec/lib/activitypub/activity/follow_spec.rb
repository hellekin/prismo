require 'rails_helper'

describe ActivityPub::Activity::Follow do
  let(:sender)    { create(:account) }
  let(:recipient) { create(:account) }

  let(:json) do
    {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: 'foo',
      type: 'Follow',
      actor: ActivityPub::TagManager.instance.uri_for(sender),
      object: ActivityPub::TagManager.instance.uri_for(recipient),
    }.with_indifferent_access
  end

  before do
    stub_jsonld_contexts!

    allow(CreateNotificationJob).to receive(:call).and_return(true)
  end

  describe '#perform' do
    subject { described_class.new(json, sender) }

    context 'when account is unlocked' do
      before { subject.perform }

      it 'creates a follow from sender to recipient' do
        expect(sender.following?(recipient)).to be true
      end

      it 'does not create a follow request' do
        expect(sender.requested_follow?(recipient)).to be false
      end

      it 'creates notification' do
        expect(CreateNotificationJob)
          .to have_received(:call)
          .with('new_follower', author: sender, recipient: recipient)
      end
    end
  end
end
