require 'rails_helper'

RSpec.describe ActivityPub::Activity::Undo do
  let(:sender) { create(:account, domain: 'example.com') }

  let(:json) do
    {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: 'foo',
      type: 'Undo',
      actor: ActivityPub::TagManager.instance.uri_for(sender),
      object: object_json,
    }.with_indifferent_access
  end

  subject { described_class.new(json, sender) }

  describe '#perform' do
    context 'with Like' do
      context 'when object is a story' do
        let(:story) { create(:story) }
        let(:object_json) do
          {
            id: 'bar',
            type: 'Like',
            actor: ActivityPub::TagManager.instance.uri_for(sender),
            object: ActivityPub::TagManager.instance.uri_for(story),
          }
        end

        before do
          create(:vote, account: sender, voteable: story)
        end

        it 'deletes vote from sender to story' do
          expect { subject.perform }
            .to change { sender.voted_on?(story) }
            .from(true)
            .to(false)
        end
      end

      context 'when object is a comment' do
        let(:comment) { create(:comment) }
        let(:object_json) do
          {
            id: 'bar',
            type: 'Like',
            actor: ActivityPub::TagManager.instance.uri_for(sender),
            object: ActivityPub::TagManager.instance.uri_for(comment),
          }
        end

        before do
          create(:vote, account: sender, voteable: comment)
        end

        it 'deletes vote from sender to comment' do
          expect { subject.perform }
            .to change { sender.voted_on?(comment) }
            .from(true)
            .to(false)
        end
      end
    end

    context 'with Follow' do
      let(:recipient) { create(:account) }

      let(:object_json) do
        {
          id: 'bar',
          type: 'Follow',
          actor: ActivityPub::TagManager.instance.uri_for(sender),
          object: ActivityPub::TagManager.instance.uri_for(recipient),
        }
      end

      before do
        sender.follow!(recipient)
      end

      it 'deletes follow from sender to recipient' do
        subject.perform
        expect(sender.following?(recipient)).to be false
      end
    end
  end
end
