class CreateUrlMeta < ActiveRecord::Migration[5.2]
  def change
    create_table :url_meta do |t|
      t.string :video_id
      t.string :title
      t.text :description
      t.string :video_provider
      t.integer :video_duration

      t.timestamps
    end
  end
end
