class CreateStories < ActiveRecord::Migration[5.2]
  def change
    create_table :stories do |t|
      t.references :account, foreign_key: true
      t.string :url, limit: 250, default: ""
      t.string :title, limit: 150, default: "", null: false
      t.text :description, limit: 16777215
      t.integer :votes_count, default: 0, null: false, unsigned: true
      t.boolean :local

      t.timestamps
    end
  end
end
