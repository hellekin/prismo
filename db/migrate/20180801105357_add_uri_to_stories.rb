class AddUriToStories < ActiveRecord::Migration[5.2]
  def change
    add_column :stories, :uri, :string
  end
end
