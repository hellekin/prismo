class AddAvatarDataToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :avatar_data, :jsonb, default: {}
  end
end
