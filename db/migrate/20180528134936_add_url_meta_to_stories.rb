class AddUrlMetaToStories < ActiveRecord::Migration[5.2]
  def change
    add_reference :stories, :url_meta, foreign_key: true
  end
end
